export class Producto{
    public selected : boolean
    public stock : string[];
    constructor(public nombre: string){
        this.stock = ['Disponible','Faltante']
    }
    isSelected() : boolean{
        return this.selected;
    }
    setSelected(s : boolean){
        this.selected = s;
    }
}