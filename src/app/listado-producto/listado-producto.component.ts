import { Component, OnInit } from '@angular/core';
import { Producto } from './../models/producto.model';

@Component({
  selector: 'app-listado-producto',
  templateUrl: './listado-producto.component.html',
  styleUrls: ['./listado-producto.component.css']
})
export class ListadoProductoComponent implements OnInit {
  productos : Producto[];
  constructor() {
    this.productos = [];
   }

  ngOnInit(): void {
  }

  guardar(nombre : string){
    // console.log(this.productos);
    this.productos.push(new Producto(nombre));
    // console.log(this.productos);
    return false;
  }

  elegido(p : Producto){
    this.productos.forEach(function (x) {x.setSelected(false)});
    p.setSelected(true);
  }

}
