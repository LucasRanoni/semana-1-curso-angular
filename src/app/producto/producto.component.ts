import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Producto } from '../models/producto.model';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Input() producto : Producto ;
  @Input('idx') position : number;
  //@HostBinding("attr.class") cssClass = 'city';
  @Output() clicked : EventEmitter<Producto>;
  constructor() { 
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.producto);
    this.producto.setSelected(true);
  }
}
